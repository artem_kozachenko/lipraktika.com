<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Weather extends Model
{
    protected $table = 'weather';
    public $timestamps = false;
    protected $fillable = [
        'city_id',
        'date',
        'time',
        'sunrise',
        'sunset',
        'weather',
        'weather_img',
        'precipitation_value',
        'precipitation_type',
        'wind_speed',
        'wind_direction',
        'temperature',
        'pressure',
        'humidity',
        'clouds',
    ];
    protected $hidden = [
        'city_id',
        'id'
    ];

    public function city()
    {
        return $this->belongsTo('App\City');
    }
}
