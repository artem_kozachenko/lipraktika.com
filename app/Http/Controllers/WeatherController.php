<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\City;
use App\Weather;
use SimpleXMLElement;
use Carbon\Carbon;

class WeatherController extends Controller
{
    public function getWeather(Request $request)
    {
        $city = City::where('name', '=', $request->input('city'))->first();
        if(!empty($city)) {
            $weatherData = $city->weather;
            $time = count($weatherData);
            $oneDayWeather = [];
            $fiveDayWeather = [];
            foreach ($weatherData as $key) {
                unset($key['city_id']);
                $date = explode(' ', $key['date']);
                isset($currentDate) ? : $currentDate = $date[1];
                if($currentDate == $date[1]) {
                    array_push($oneDayWeather, $key);
                }
                else {
                    array_push($fiveDayWeather, $oneDayWeather);
                    $oneDayWeather = [];
                    array_push($oneDayWeather, $key);
                    $currentDate = $date[1];
                }
                $time--;
                if($time == 0 && count($fiveDayWeather) != 5) {
                    array_push($fiveDayWeather, $oneDayWeather);
                    break;
                }
                if(count($fiveDayWeather) == 5) {
                    break;
                }
            }
            $message = 'Weather forecast from database.';
            $data = [$message, $fiveDayWeather];
            echo json_encode($data);
        }
        else {
            $weather = @file_get_contents('http://api.openweathermap.org/data/2.5/forecast?q=' . $request->input('city') . '&mode=xml&units=metric&appid=4e55ca5cebd722a7da369cae6229d09a');
            if($http_response_header[0] != 'HTTP/1.1 404 Not Found') {
                $weather = new SimpleXMLElement($weather);
                $weather = json_decode(json_encode((array)$weather), TRUE);
                $city = new City;
                $city->name = $weather['location']['name'];
                $city->save();
                $sunrise = explode('T', $weather['sun']['@attributes']['rise']);
                $sunrise = $sunrise[1];
                $sunset = explode('T', $weather['sun']['@attributes']['set']);
                $sunset = $sunset[1];
                $weatherData = [];
                foreach ($weather['forecast']['time'] as $key => $value) {
                    $date = explode('T', $value['@attributes']['from']);
                    $row = array(
                        'city_id' => $city->id,
                        'date' => strftime("%a %d %b", strtotime($date[0])),
                        'time' => $date[1],
                        'sunrise' => $sunrise,
                        'sunset' => $sunset,
                        'weather' => $value['symbol']['@attributes']['name'],
                        'weather_img' => 'http://openweathermap.org/img/w/' . $value['symbol']['@attributes']['var'] . '.png',
                        'precipitation_value' => isset($value['precipitation']['@attributes']['value']) ? $value['precipitation']['@attributes']['value'] : null,
                        'precipitation_type' => isset($value['precipitation']['@attributes']['type']) ? $value['precipitation']['@attributes']['type'] : null,
                        'wind_speed' => $value['windSpeed']['@attributes']['mps'],
                        'wind_direction' => $value['windDirection']['@attributes']['name'] != '' ? $value['windDirection']['@attributes']['name'] : null,
                        'temperature' => $value['temperature']['@attributes']['value'],
                        'pressure' => $value['pressure']['@attributes']['value'],
                        'humidity' => $value['humidity']['@attributes']['value'],
                        'clouds' => $value['clouds']['@attributes']['all']
                    );
                    array_push($weatherData, $row);
                }
                $time = count($weatherData);
                $oneDayWeather = [];
                $fiveDayWeather = [];
                foreach ($weatherData as $key) {
                    unset($key['city_id']);
                    $date = explode(' ', $key['date']);
                    isset($currentDate) ? : $currentDate = $date[1];
                    if($currentDate == $date[1]) {
                        array_push($oneDayWeather, $key);
                    }
                    else {
                        array_push($fiveDayWeather, $oneDayWeather);
                        $oneDayWeather = [];
                        array_push($oneDayWeather, $key);
                        $currentDate = $date[1];
                    }
                    $time--;
                    if($time == 0 && count($fiveDayWeather) != 5) {
                        array_push($fiveDayWeather, $oneDayWeather);
                        break;
                    }
                    if(count($fiveDayWeather) == 5) {
                        break;
                    }
                }
                $message = 'Weather forecast from Api.';
                $data = [$message, $fiveDayWeather];
                echo json_encode($data);
                Weather::insert($weatherData);
            }
            else {
                echo json_encode(0);
                exit();
            }
        }
    }
    public function deleteOldRecords()
    {
        $cities = City::whereDate('created_at', '<=', Carbon::now()->subDay()->toDateTimeString())->get();
        $cities_ids = [];
        $weather_ids = [];
        foreach ($cities as $key) {
            array_push($cities_ids, $key['attributes']['id']);
            foreach ($key->weather as $key) {
                array_push($weather_ids, $key['attributes']['id']);
            }
        }
        City::destroy($cities_ids);
        Weather::destroy($weather_ids);
        return redirect('/');
    }
}
