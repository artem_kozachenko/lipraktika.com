<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Weather</title>
        <link rel="shortcut icon" src="{{ asset('favicon.ico') }}" type="image/x-icon">
        <link rel="stylesheet" href="{{ asset('bootstrap/css/bootstrap.min.css') }}">
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
        <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    </head>
    <body>
        <div class="container">
            <div id="form_div" class="form-group">
                <form id="city_form" method="get" action="{{ route('getWeather') }}">
                    {{ csrf_field() }}
                    <label for="city">City</label>
                    <input class="form-control" id="city" type="text" name="city" placeholder="Enter city name">
                    <button type="submit" class="btn btn-primary form-control">Get weather</button>
                    <p id="message"></p>
                </form>
                <br>
                <a href="{{ route('deleteWeather') }}" class="btn btn-danger form-control" title="Records older then 1 day.">Delete old records</a>
            </div>
            <div id="preloader">
                <i class="fa fa-refresh fa-spin fa-3x fa-fw"></i>
            </div>
            <ul class="nav nav-tabs nav-justified" role="tablist">
                <li class="disabled"><a id="tab_1" href="#day_0" role="tab" data-toggle="pill">Day 1</a></li>
                <li class="disabled"><a id="tab_2" href="#day_1" role="tab" data-toggle="pill">Day 2</a></li>
                <li class="disabled"><a id="tab_3" href="#day_2" role="tab" data-toggle="pill">Day 3</a></li>
                <li class="disabled"><a id="tab_4" href="#day_3" role="tab" data-toggle="pill">Day 4</a></li>
                <li class="disabled"><a id="tab_5" href="#day_4" role="tab" data-toggle="pill">Day 5</a></li>
            </ul>
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane text-center" id="day_0">
                    <div class="sun"></div>
                    <div class="time-div time-0" hidden>
                        <p class="weather-time text-center"></p>
                        <hr>
                        <p class="bold">Weather</p>
                        <img name="weather" class="text-center" src="" data-toggle="tooltip" data-placement="top" title=""></img>
                        <p class="bold">Temperature</p>
                        <p class="temperature"></p>
                        <p class="bold">Clouds</p>
                        <p class="clouds"></p>
                        <p class="bold">Wind</p>
                        <img name="wind" class="text-center" src="" data-toggle="tooltip" data-placement="top" title=""></img>
                        <img name="wind-2" class="text-center" src="" data-toggle="tooltip" data-placement="top" title="" hidden></img>
                        <p class="wind-speed"></p>
                        <p class="bold">Precipitation</p>
                        <p class="precipitation-type"></p>
                        <p class="precipitation-value"></p>
                        <p class="bold">Pressure</p>
                        <p class="pressure"></p>
                        <p class="bold">Humidity</p>
                        <p class="humidity"></p>
                    </div>
                    <div class="time-div time-1" hidden>
                        <p class="weather-time text-center"></p>
                        <hr>
                        <p class="bold">Weather</p>
                        <img name="weather" class="text-center" src="" data-toggle="tooltip" data-placement="top" title=""></img>
                        <p class="bold">Temperature</p>
                        <p class="temperature"></p>
                        <p class="bold">Clouds</p>
                        <p class="clouds"></p>
                        <p class="bold">Wind</p>
                        <img name="wind" class="text-center" src="" data-toggle="tooltip" data-placement="top" title=""></img>
                        <img name="wind-2" class="text-center" src="" data-toggle="tooltip" data-placement="top" title="" hidden></img>
                        <p class="wind-speed"></p>
                        <p class="bold">Precipitation</p>
                        <p class="precipitation-type"></p>
                        <p class="precipitation-value"></p>
                        <p class="bold">Pressure</p>
                        <p class="pressure"></p>
                        <p class="bold">Humidity</p>
                        <p class="humidity"></p>
                    </div>
                    <div class="time-div time-2" hidden>
                        <p class="weather-time text-center"></p>
                        <hr>
                        <p class="bold">Weather</p>
                        <img name="weather" class="text-center" src="" data-toggle="tooltip" data-placement="top" title=""></img>
                        <p class="bold">Temperature</p>
                        <p class="temperature"></p>
                        <p class="bold">Clouds</p>
                        <p class="clouds"></p>
                        <p class="bold">Wind</p>
                        <img name="wind" class="text-center" src="" data-toggle="tooltip" data-placement="top" title=""></img>
                        <img name="wind-2" class="text-center" src="" data-toggle="tooltip" data-placement="top" title="" hidden></img>
                        <p class="wind-speed"></p>
                        <p class="bold">Precipitation</p>
                        <p class="precipitation-type"></p>
                        <p class="precipitation-value"></p>
                        <p class="bold">Pressure</p>
                        <p class="pressure"></p>
                        <p class="bold">Humidity</p>
                        <p class="humidity"></p>
                    </div>
                    <div class="time-div time-3" hidden>
                        <p class="weather-time text-center"></p>
                        <hr>
                        <p class="bold">Weather</p>
                        <img name="weather" class="text-center" src="" data-toggle="tooltip" data-placement="top" title=""></img>
                        <p class="bold">Temperature</p>
                        <p class="temperature"></p>
                        <p class="bold">Clouds</p>
                        <p class="clouds"></p>
                        <p class="bold">Wind</p>
                        <img name="wind" class="text-center" src="" data-toggle="tooltip" data-placement="top" title=""></img>
                        <img name="wind-2" class="text-center" src="" data-toggle="tooltip" data-placement="top" title="" hidden></img>
                        <p class="wind-speed"></p>
                        <p class="bold">Precipitation</p>
                        <p class="precipitation-type"></p>
                        <p class="precipitation-value"></p>
                        <p class="bold">Pressure</p>
                        <p class="pressure"></p>
                        <p class="bold">Humidity</p>
                        <p class="humidity"></p>
                    </div>
                    <div class="time-div time-4" hidden>
                        <p class="weather-time text-center"></p>
                        <hr>
                        <p class="bold">Weather</p>
                        <img name="weather" class="text-center" src="" data-toggle="tooltip" data-placement="top" title=""></img>
                        <p class="bold">Temperature</p>
                        <p class="temperature"></p>
                        <p class="bold">Clouds</p>
                        <p class="clouds"></p>
                        <p class="bold">Wind</p>
                        <img name="wind" class="text-center" src="" data-toggle="tooltip" data-placement="top" title=""></img>
                        <img name="wind-2" class="text-center" src="" data-toggle="tooltip" data-placement="top" title="" hidden></img>
                        <p class="wind-speed"></p>
                        <p class="bold">Precipitation</p>
                        <p class="precipitation-type"></p>
                        <p class="precipitation-value"></p>
                        <p class="bold">Pressure</p>
                        <p class="pressure"></p>
                        <p class="bold">Humidity</p>
                        <p class="humidity"></p>
                    </div>
                    <div class="time-div time-5" hidden>
                        <p class="weather-time text-center"></p>
                        <hr>
                        <p class="bold">Weather</p>
                        <img name="weather" class="text-center" src="" data-toggle="tooltip" data-placement="top" title=""></img>
                        <p class="bold">Temperature</p>
                        <p class="temperature"></p>
                        <p class="bold">Clouds</p>
                        <p class="clouds"></p>
                        <p class="bold">Wind</p>
                        <img name="wind" class="text-center" src="" data-toggle="tooltip" data-placement="top" title=""></img>
                        <img name="wind-2" class="text-center" src="" data-toggle="tooltip" data-placement="top" title="" hidden></img>
                        <p class="wind-speed"></p>
                        <p class="bold">Precipitation</p>
                        <p class="precipitation-type"></p>
                        <p class="precipitation-value"></p>
                        <p class="bold">Pressure</p>
                        <p class="pressure"></p>
                        <p class="bold">Humidity</p>
                        <p class="humidity"></p>
                    </div>
                    <div class="time-div time-6" hidden>
                        <p class="weather-time text-center"></p>
                        <hr>
                        <p class="bold">Weather</p>
                        <img name="weather" class="text-center" src="" data-toggle="tooltip" data-placement="top" title=""></img>
                        <p class="bold">Temperature</p>
                        <p class="temperature"></p>
                        <p class="bold">Clouds</p>
                        <p class="clouds"></p>
                        <p class="bold">Wind</p>
                        <img name="wind" class="text-center" src="" data-toggle="tooltip" data-placement="top" title=""></img>
                        <img name="wind-2" class="text-center" src="" data-toggle="tooltip" data-placement="top" title="" hidden></img>
                        <p class="wind-speed"></p>
                        <p class="bold">Precipitation</p>
                        <p class="precipitation-type"></p>
                        <p class="precipitation-value"></p>
                        <p class="bold">Pressure</p>
                        <p class="pressure"></p>
                        <p class="bold">Humidity</p>
                        <p class="humidity"></p>
                    </div>
                    <div class="time-div time-7" hidden>
                        <p class="weather-time text-center"></p>
                        <hr>
                        <p class="bold">Weather</p>
                        <img name="weather" class="text-center" src="" data-toggle="tooltip" data-placement="top" title=""></img>
                        <p class="bold">Temperature</p>
                        <p class="temperature"></p>
                        <p class="bold">Clouds</p>
                        <p class="clouds"></p>
                        <p class="bold">Wind</p>
                        <img name="wind" class="text-center" src="" data-toggle="tooltip" data-placement="top" title=""></img>
                        <img name="wind-2" class="text-center" src="" data-toggle="tooltip" data-placement="top" title="" hidden></img>
                        <p class="wind-speed"></p>
                        <p class="bold">Precipitation</p>
                        <p class="precipitation-type"></p>
                        <p class="precipitation-value"></p>
                        <p class="bold">Pressure</p>
                        <p class="pressure"></p>
                        <p class="bold">Humidity</p>
                        <p class="humidity"></p>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane text-center" id="day_1">
                    <div class="sun"></div>
                    <div class="time-div time-0" hidden>
                        <p class="weather-time text-center"></p>
                        <hr>
                        <p class="bold">Weather</p>
                        <img name="weather" class="text-center" src="" data-toggle="tooltip" data-placement="top" title=""></img>
                        <p class="bold">Temperature</p>
                        <p class="temperature"></p>
                        <p class="bold">Clouds</p>
                        <p class="clouds"></p>
                        <p class="bold">Wind</p>
                        <img name="wind" class="text-center" src="" data-toggle="tooltip" data-placement="top" title=""></img>
                        <img name="wind-2" class="text-center" src="" data-toggle="tooltip" data-placement="top" title="" hidden></img>
                        <p class="wind-speed"></p>
                        <p class="bold">Precipitation</p>
                        <p class="precipitation-type"></p>
                        <p class="precipitation-value"></p>
                        <p class="bold">Pressure</p>
                        <p class="pressure"></p>
                        <p class="bold">Humidity</p>
                        <p class="humidity"></p>
                    </div>
                    <div class="time-div time-1" hidden>
                        <p class="weather-time text-center"></p>
                        <hr>
                        <p class="bold">Weather</p>
                        <img name="weather" class="text-center" src="" data-toggle="tooltip" data-placement="top" title=""></img>
                        <p class="bold">Temperature</p>
                        <p class="temperature"></p>
                        <p class="bold">Clouds</p>
                        <p class="clouds"></p>
                        <p class="bold">Wind</p>
                        <img name="wind" class="text-center" src="" data-toggle="tooltip" data-placement="top" title=""></img>
                        <img name="wind-2" class="text-center" src="" data-toggle="tooltip" data-placement="top" title="" hidden></img>
                        <p class="wind-speed"></p>
                        <p class="bold">Precipitation</p>
                        <p class="precipitation-type"></p>
                        <p class="precipitation-value"></p>
                        <p class="bold">Pressure</p>
                        <p class="pressure"></p>
                        <p class="bold">Humidity</p>
                        <p class="humidity"></p>
                    </div>
                    <div class="time-div time-2" hidden>
                        <p class="weather-time text-center"></p>
                        <hr>
                        <p class="bold">Weather</p>
                        <img name="weather" class="text-center" src="" data-toggle="tooltip" data-placement="top" title=""></img>
                        <p class="bold">Temperature</p>
                        <p class="temperature"></p>
                        <p class="bold">Clouds</p>
                        <p class="clouds"></p>
                        <p class="bold">Wind</p>
                        <img name="wind" class="text-center" src="" data-toggle="tooltip" data-placement="top" title=""></img>
                        <img name="wind-2" class="text-center" src="" data-toggle="tooltip" data-placement="top" title="" hidden></img>
                        <p class="wind-speed"></p>
                        <p class="bold">Precipitation</p>
                        <p class="precipitation-type"></p>
                        <p class="precipitation-value"></p>
                        <p class="bold">Pressure</p>
                        <p class="pressure"></p>
                        <p class="bold">Humidity</p>
                        <p class="humidity"></p>
                    </div>
                    <div class="time-div time-3" hidden>
                        <p class="weather-time text-center"></p>
                        <hr>
                        <p class="bold">Weather</p>
                        <img name="weather" class="text-center" src="" data-toggle="tooltip" data-placement="top" title=""></img>
                        <p class="bold">Temperature</p>
                        <p class="temperature"></p>
                        <p class="bold">Clouds</p>
                        <p class="clouds"></p>
                        <p class="bold">Wind</p>
                        <img name="wind" class="text-center" src="" data-toggle="tooltip" data-placement="top" title=""></img>
                        <img name="wind-2" class="text-center" src="" data-toggle="tooltip" data-placement="top" title="" hidden></img>
                        <p class="wind-speed"></p>
                        <p class="bold">Precipitation</p>
                        <p class="precipitation-type"></p>
                        <p class="precipitation-value"></p>
                        <p class="bold">Pressure</p>
                        <p class="pressure"></p>
                        <p class="bold">Humidity</p>
                        <p class="humidity"></p>
                    </div>
                    <div class="time-div time-4" hidden>
                        <p class="weather-time text-center"></p>
                        <hr>
                        <p class="bold">Weather</p>
                        <img name="weather" class="text-center" src="" data-toggle="tooltip" data-placement="top" title=""></img>
                        <p class="bold">Temperature</p>
                        <p class="temperature"></p>
                        <p class="bold">Clouds</p>
                        <p class="clouds"></p>
                        <p class="bold">Wind</p>
                        <img name="wind" class="text-center" src="" data-toggle="tooltip" data-placement="top" title=""></img>
                        <img name="wind-2" class="text-center" src="" data-toggle="tooltip" data-placement="top" title="" hidden></img>
                        <p class="wind-speed"></p>
                        <p class="bold">Precipitation</p>
                        <p class="precipitation-type"></p>
                        <p class="precipitation-value"></p>
                        <p class="bold">Pressure</p>
                        <p class="pressure"></p>
                        <p class="bold">Humidity</p>
                        <p class="humidity"></p>
                    </div>
                    <div class="time-div time-5" hidden>
                        <p class="weather-time text-center"></p>
                        <hr>
                        <p class="bold">Weather</p>
                        <img name="weather" class="text-center" src="" data-toggle="tooltip" data-placement="top" title=""></img>
                        <p class="bold">Temperature</p>
                        <p class="temperature"></p>
                        <p class="bold">Clouds</p>
                        <p class="clouds"></p>
                        <p class="bold">Wind</p>
                        <img name="wind" class="text-center" src="" data-toggle="tooltip" data-placement="top" title=""></img>
                        <img name="wind-2" class="text-center" src="" data-toggle="tooltip" data-placement="top" title="" hidden></img>
                        <p class="wind-speed"></p>
                        <p class="bold">Precipitation</p>
                        <p class="precipitation-type"></p>
                        <p class="precipitation-value"></p>
                        <p class="bold">Pressure</p>
                        <p class="pressure"></p>
                        <p class="bold">Humidity</p>
                        <p class="humidity"></p>
                    </div>
                    <div class="time-div time-6" hidden>
                        <p class="weather-time text-center"></p>
                        <hr>
                        <p class="bold">Weather</p>
                        <img name="weather" class="text-center" src="" data-toggle="tooltip" data-placement="top" title=""></img>
                        <p class="bold">Temperature</p>
                        <p class="temperature"></p>
                        <p class="bold">Clouds</p>
                        <p class="clouds"></p>
                        <p class="bold">Wind</p>
                        <img name="wind" class="text-center" src="" data-toggle="tooltip" data-placement="top" title=""></img>
                        <img name="wind-2" class="text-center" src="" data-toggle="tooltip" data-placement="top" title="" hidden></img>
                        <p class="wind-speed"></p>
                        <p class="bold">Precipitation</p>
                        <p class="precipitation-type"></p>
                        <p class="precipitation-value"></p>
                        <p class="bold">Pressure</p>
                        <p class="pressure"></p>
                        <p class="bold">Humidity</p>
                        <p class="humidity"></p>
                    </div>
                    <div class="time-div time-7" hidden>
                        <p class="weather-time text-center"></p>
                        <hr>
                        <p class="bold">Weather</p>
                        <img name="weather" class="text-center" src="" data-toggle="tooltip" data-placement="top" title=""></img>
                        <p class="bold">Temperature</p>
                        <p class="temperature"></p>
                        <p class="bold">Clouds</p>
                        <p class="clouds"></p>
                        <p class="bold">Wind</p>
                        <img name="wind" class="text-center" src="" data-toggle="tooltip" data-placement="top" title=""></img>
                        <img name="wind-2" class="text-center" src="" data-toggle="tooltip" data-placement="top" title="" hidden></img>
                        <p class="wind-speed"></p>
                        <p class="bold">Precipitation</p>
                        <p class="precipitation-type"></p>
                        <p class="precipitation-value"></p>
                        <p class="bold">Pressure</p>
                        <p class="pressure"></p>
                        <p class="bold">Humidity</p>
                        <p class="humidity"></p>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane text-center" id="day_2">
                    <div class="sun"></div>
                    <div class="time-div time-0" hidden>
                        <p class="weather-time text-center"></p>
                        <hr>
                        <p class="bold">Weather</p>
                        <img name="weather" class="text-center" src="" data-toggle="tooltip" data-placement="top" title=""></img>
                        <p class="bold">Temperature</p>
                        <p class="temperature"></p>
                        <p class="bold">Clouds</p>
                        <p class="clouds"></p>
                        <p class="bold">Wind</p>
                        <img name="wind" class="text-center" src="" data-toggle="tooltip" data-placement="top" title=""></img>
                        <img name="wind-2" class="text-center" src="" data-toggle="tooltip" data-placement="top" title="" hidden></img>
                        <p class="wind-speed"></p>
                        <p class="bold">Precipitation</p>
                        <p class="precipitation-type"></p>
                        <p class="precipitation-value"></p>
                        <p class="bold">Pressure</p>
                        <p class="pressure"></p>
                        <p class="bold">Humidity</p>
                        <p class="humidity"></p>
                    </div>
                    <div class="time-div time-1" hidden>
                        <p class="weather-time text-center"></p>
                        <hr>
                        <p class="bold">Weather</p>
                        <img name="weather" class="text-center" src="" data-toggle="tooltip" data-placement="top" title=""></img>
                        <p class="bold">Temperature</p>
                        <p class="temperature"></p>
                        <p class="bold">Clouds</p>
                        <p class="clouds"></p>
                        <p class="bold">Wind</p>
                        <img name="wind" class="text-center" src="" data-toggle="tooltip" data-placement="top" title=""></img>
                        <img name="wind-2" class="text-center" src="" data-toggle="tooltip" data-placement="top" title="" hidden></img>
                        <p class="wind-speed"></p>
                        <p class="bold">Precipitation</p>
                        <p class="precipitation-type"></p>
                        <p class="precipitation-value"></p>
                        <p class="bold">Pressure</p>
                        <p class="pressure"></p>
                        <p class="bold">Humidity</p>
                        <p class="humidity"></p>
                    </div>
                    <div class="time-div time-2" hidden>
                        <p class="weather-time text-center"></p>
                        <hr>
                        <p class="bold">Weather</p>
                        <img name="weather" class="text-center" src="" data-toggle="tooltip" data-placement="top" title=""></img>
                        <p class="bold">Temperature</p>
                        <p class="temperature"></p>
                        <p class="bold">Clouds</p>
                        <p class="clouds"></p>
                        <p class="bold">Wind</p>
                        <img name="wind" class="text-center" src="" data-toggle="tooltip" data-placement="top" title=""></img>
                        <img name="wind-2" class="text-center" src="" data-toggle="tooltip" data-placement="top" title="" hidden></img>
                        <p class="wind-speed"></p>
                        <p class="bold">Precipitation</p>
                        <p class="precipitation-type"></p>
                        <p class="precipitation-value"></p>
                        <p class="bold">Pressure</p>
                        <p class="pressure"></p>
                        <p class="bold">Humidity</p>
                        <p class="humidity"></p>
                    </div>
                    <div class="time-div time-3" hidden>
                        <p class="weather-time text-center"></p>
                        <hr>
                        <p class="bold">Weather</p>
                        <img name="weather" class="text-center" src="" data-toggle="tooltip" data-placement="top" title=""></img>
                        <p class="bold">Temperature</p>
                        <p class="temperature"></p>
                        <p class="bold">Clouds</p>
                        <p class="clouds"></p>
                        <p class="bold">Wind</p>
                        <img name="wind" class="text-center" src="" data-toggle="tooltip" data-placement="top" title=""></img>
                        <img name="wind-2" class="text-center" src="" data-toggle="tooltip" data-placement="top" title="" hidden></img>
                        <p class="wind-speed"></p>
                        <p class="bold">Precipitation</p>
                        <p class="precipitation-type"></p>
                        <p class="precipitation-value"></p>
                        <p class="bold">Pressure</p>
                        <p class="pressure"></p>
                        <p class="bold">Humidity</p>
                        <p class="humidity"></p>
                    </div>
                    <div class="time-div time-4" hidden>
                        <p class="weather-time text-center"></p>
                        <hr>
                        <p class="bold">Weather</p>
                        <img name="weather" class="text-center" src="" data-toggle="tooltip" data-placement="top" title=""></img>
                        <p class="bold">Temperature</p>
                        <p class="temperature"></p>
                        <p class="bold">Clouds</p>
                        <p class="clouds"></p>
                        <p class="bold">Wind</p>
                        <img name="wind" class="text-center" src="" data-toggle="tooltip" data-placement="top" title=""></img>
                        <img name="wind-2" class="text-center" src="" data-toggle="tooltip" data-placement="top" title="" hidden></img>
                        <p class="wind-speed"></p>
                        <p class="bold">Precipitation</p>
                        <p class="precipitation-type"></p>
                        <p class="precipitation-value"></p>
                        <p class="bold">Pressure</p>
                        <p class="pressure"></p>
                        <p class="bold">Humidity</p>
                        <p class="humidity"></p>
                    </div>
                    <div class="time-div time-5" hidden>
                        <p class="weather-time text-center"></p>
                        <hr>
                        <p class="bold">Weather</p>
                        <img name="weather" class="text-center" src="" data-toggle="tooltip" data-placement="top" title=""></img>
                        <p class="bold">Temperature</p>
                        <p class="temperature"></p>
                        <p class="bold">Clouds</p>
                        <p class="clouds"></p>
                        <p class="bold">Wind</p>
                        <img name="wind" class="text-center" src="" data-toggle="tooltip" data-placement="top" title=""></img>
                        <img name="wind-2" class="text-center" src="" data-toggle="tooltip" data-placement="top" title="" hidden></img>
                        <p class="wind-speed"></p>
                        <p class="bold">Precipitation</p>
                        <p class="precipitation-type"></p>
                        <p class="precipitation-value"></p>
                        <p class="bold">Pressure</p>
                        <p class="pressure"></p>
                        <p class="bold">Humidity</p>
                        <p class="humidity"></p>
                    </div>
                    <div class="time-div time-6" hidden>
                        <p class="weather-time text-center"></p>
                        <hr>
                        <p class="bold">Weather</p>
                        <img name="weather" class="text-center" src="" data-toggle="tooltip" data-placement="top" title=""></img>
                        <p class="bold">Temperature</p>
                        <p class="temperature"></p>
                        <p class="bold">Clouds</p>
                        <p class="clouds"></p>
                        <p class="bold">Wind</p>
                        <img name="wind" class="text-center" src="" data-toggle="tooltip" data-placement="top" title=""></img>
                        <img name="wind-2" class="text-center" src="" data-toggle="tooltip" data-placement="top" title="" hidden></img>
                        <p class="wind-speed"></p>
                        <p class="bold">Precipitation</p>
                        <p class="precipitation-type"></p>
                        <p class="precipitation-value"></p>
                        <p class="bold">Pressure</p>
                        <p class="pressure"></p>
                        <p class="bold">Humidity</p>
                        <p class="humidity"></p>
                    </div>
                    <div class="time-div time-7" hidden>
                        <p class="weather-time text-center"></p>
                        <hr>
                        <p class="bold">Weather</p>
                        <img name="weather" class="text-center" src="" data-toggle="tooltip" data-placement="top" title=""></img>
                        <p class="bold">Temperature</p>
                        <p class="temperature"></p>
                        <p class="bold">Clouds</p>
                        <p class="clouds"></p>
                        <p class="bold">Wind</p>
                        <img name="wind" class="text-center" src="" data-toggle="tooltip" data-placement="top" title=""></img>
                        <img name="wind-2" class="text-center" src="" data-toggle="tooltip" data-placement="top" title="" hidden></img>
                        <p class="wind-speed"></p>
                        <p class="bold">Precipitation</p>
                        <p class="precipitation-type"></p>
                        <p class="precipitation-value"></p>
                        <p class="bold">Pressure</p>
                        <p class="pressure"></p>
                        <p class="bold">Humidity</p>
                        <p class="humidity"></p>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane text-center" id="day_3">
                    <div class="sun"></div>
                    <div class="time-div time-0" hidden>
                        <p class="weather-time text-center"></p>
                        <hr>
                        <p class="bold">Weather</p>
                        <img name="weather" class="text-center" src="" data-toggle="tooltip" data-placement="top" title=""></img>
                        <p class="bold">Temperature</p>
                        <p class="temperature"></p>
                        <p class="bold">Clouds</p>
                        <p class="clouds"></p>
                        <p class="bold">Wind</p>
                        <img name="wind" class="text-center" src="" data-toggle="tooltip" data-placement="top" title=""></img>
                        <img name="wind-2" class="text-center" src="" data-toggle="tooltip" data-placement="top" title="" hidden></img>
                        <p class="wind-speed"></p>
                        <p class="bold">Precipitation</p>
                        <p class="precipitation-type"></p>
                        <p class="precipitation-value"></p>
                        <p class="bold">Pressure</p>
                        <p class="pressure"></p>
                        <p class="bold">Humidity</p>
                        <p class="humidity"></p>
                    </div>
                    <div class="time-div time-1" hidden>
                        <p class="weather-time text-center"></p>
                        <hr>
                        <p class="bold">Weather</p>
                        <img name="weather" class="text-center" src="" data-toggle="tooltip" data-placement="top" title=""></img>
                        <p class="bold">Temperature</p>
                        <p class="temperature"></p>
                        <p class="bold">Clouds</p>
                        <p class="clouds"></p>
                        <p class="bold">Wind</p>
                        <img name="wind" class="text-center" src="" data-toggle="tooltip" data-placement="top" title=""></img>
                        <img name="wind-2" class="text-center" src="" data-toggle="tooltip" data-placement="top" title="" hidden></img>
                        <p class="wind-speed"></p>
                        <p class="bold">Precipitation</p>
                        <p class="precipitation-type"></p>
                        <p class="precipitation-value"></p>
                        <p class="bold">Pressure</p>
                        <p class="pressure"></p>
                        <p class="bold">Humidity</p>
                        <p class="humidity"></p>
                    </div>
                    <div class="time-div time-2" hidden>
                        <p class="weather-time text-center"></p>
                        <hr>
                        <p class="bold">Weather</p>
                        <img name="weather" class="text-center" src="" data-toggle="tooltip" data-placement="top" title=""></img>
                        <p class="bold">Temperature</p>
                        <p class="temperature"></p>
                        <p class="bold">Clouds</p>
                        <p class="clouds"></p>
                        <p class="bold">Wind</p>
                        <img name="wind" class="text-center" src="" data-toggle="tooltip" data-placement="top" title=""></img>
                        <img name="wind-2" class="text-center" src="" data-toggle="tooltip" data-placement="top" title="" hidden></img>
                        <p class="wind-speed"></p>
                        <p class="bold">Precipitation</p>
                        <p class="precipitation-type"></p>
                        <p class="precipitation-value"></p>
                        <p class="bold">Pressure</p>
                        <p class="pressure"></p>
                        <p class="bold">Humidity</p>
                        <p class="humidity"></p>
                    </div>
                    <div class="time-div time-3" hidden>
                        <p class="weather-time text-center"></p>
                        <hr>
                        <p class="bold">Weather</p>
                        <img name="weather" class="text-center" src="" data-toggle="tooltip" data-placement="top" title=""></img>
                        <p class="bold">Temperature</p>
                        <p class="temperature"></p>
                        <p class="bold">Clouds</p>
                        <p class="clouds"></p>
                        <p class="bold">Wind</p>
                        <img name="wind" class="text-center" src="" data-toggle="tooltip" data-placement="top" title=""></img>
                        <img name="wind-2" class="text-center" src="" data-toggle="tooltip" data-placement="top" title="" hidden></img>
                        <p class="wind-speed"></p>
                        <p class="bold">Precipitation</p>
                        <p class="precipitation-type"></p>
                        <p class="precipitation-value"></p>
                        <p class="bold">Pressure</p>
                        <p class="pressure"></p>
                        <p class="bold">Humidity</p>
                        <p class="humidity"></p>
                    </div>
                    <div class="time-div time-4" hidden>
                        <p class="weather-time text-center"></p>
                        <hr>
                        <p class="bold">Weather</p>
                        <img name="weather" class="text-center" src="" data-toggle="tooltip" data-placement="top" title=""></img>
                        <p class="bold">Temperature</p>
                        <p class="temperature"></p>
                        <p class="bold">Clouds</p>
                        <p class="clouds"></p>
                        <p class="bold">Wind</p>
                        <img name="wind" class="text-center" src="" data-toggle="tooltip" data-placement="top" title=""></img>
                        <img name="wind-2" class="text-center" src="" data-toggle="tooltip" data-placement="top" title="" hidden></img>
                        <p class="wind-speed"></p>
                        <p class="bold">Precipitation</p>
                        <p class="precipitation-type"></p>
                        <p class="precipitation-value"></p>
                        <p class="bold">Pressure</p>
                        <p class="pressure"></p>
                        <p class="bold">Humidity</p>
                        <p class="humidity"></p>
                    </div>
                    <div class="time-div time-5" hidden>
                        <p class="weather-time text-center"></p>
                        <hr>
                        <p class="bold">Weather</p>
                        <img name="weather" class="text-center" src="" data-toggle="tooltip" data-placement="top" title=""></img>
                        <p class="bold">Temperature</p>
                        <p class="temperature"></p>
                        <p class="bold">Clouds</p>
                        <p class="clouds"></p>
                        <p class="bold">Wind</p>
                        <img name="wind" class="text-center" src="" data-toggle="tooltip" data-placement="top" title=""></img>
                        <img name="wind-2" class="text-center" src="" data-toggle="tooltip" data-placement="top" title="" hidden></img>
                        <p class="wind-speed"></p>
                        <p class="bold">Precipitation</p>
                        <p class="precipitation-type"></p>
                        <p class="precipitation-value"></p>
                        <p class="bold">Pressure</p>
                        <p class="pressure"></p>
                        <p class="bold">Humidity</p>
                        <p class="humidity"></p>
                    </div>
                    <div class="time-div time-6" hidden>
                        <p class="weather-time text-center"></p>
                        <hr>
                        <p class="bold">Weather</p>
                        <img name="weather" class="text-center" src="" data-toggle="tooltip" data-placement="top" title=""></img>
                        <p class="bold">Temperature</p>
                        <p class="temperature"></p>
                        <p class="bold">Clouds</p>
                        <p class="clouds"></p>
                        <p class="bold">Wind</p>
                        <img name="wind" class="text-center" src="" data-toggle="tooltip" data-placement="top" title=""></img>
                        <img name="wind-2" class="text-center" src="" data-toggle="tooltip" data-placement="top" title="" hidden></img>
                        <p class="wind-speed"></p>
                        <p class="bold">Precipitation</p>
                        <p class="precipitation-type"></p>
                        <p class="precipitation-value"></p>
                        <p class="bold">Pressure</p>
                        <p class="pressure"></p>
                        <p class="bold">Humidity</p>
                        <p class="humidity"></p>
                    </div>
                    <div class="time-div time-7" hidden>
                        <p class="weather-time text-center"></p>
                        <hr>
                        <p class="bold">Weather</p>
                        <img name="weather" class="text-center" src="" data-toggle="tooltip" data-placement="top" title=""></img>
                        <p class="bold">Temperature</p>
                        <p class="temperature"></p>
                        <p class="bold">Clouds</p>
                        <p class="clouds"></p>
                        <p class="bold">Wind</p>
                        <img name="wind" class="text-center" src="" data-toggle="tooltip" data-placement="top" title=""></img>
                        <img name="wind-2" class="text-center" src="" data-toggle="tooltip" data-placement="top" title="" hidden></img>
                        <p class="wind-speed"></p>
                        <p class="bold">Precipitation</p>
                        <p class="precipitation-type"></p>
                        <p class="precipitation-value"></p>
                        <p class="bold">Pressure</p>
                        <p class="pressure"></p>
                        <p class="bold">Humidity</p>
                        <p class="humidity"></p>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane text-center" id="day_4">
                    <div class="sun"></div>
                    <div class="time-div time-0" hidden>
                        <p class="weather-time text-center"></p>
                        <hr>
                        <p class="bold">Weather</p>
                        <img name="weather" class="text-center" src="" data-toggle="tooltip" data-placement="top" title=""></img>
                        <p class="bold">Temperature</p>
                        <p class="temperature"></p>
                        <p class="bold">Clouds</p>
                        <p class="clouds"></p>
                        <p class="bold">Wind</p>
                        <img name="wind" class="text-center" src="" data-toggle="tooltip" data-placement="top" title=""></img>
                        <img name="wind-2" class="text-center" src="" data-toggle="tooltip" data-placement="top" title="" hidden></img>
                        <p class="wind-speed"></p>
                        <p class="bold">Precipitation</p>
                        <p class="precipitation-type"></p>
                        <p class="precipitation-value"></p>
                        <p class="bold">Pressure</p>
                        <p class="pressure"></p>
                        <p class="bold">Humidity</p>
                        <p class="humidity"></p>
                    </div>
                    <div class="time-div time-1" hidden>
                        <p class="weather-time text-center"></p>
                        <hr>
                        <p class="bold">Weather</p>
                        <img name="weather" class="text-center" src="" data-toggle="tooltip" data-placement="top" title=""></img>
                        <p class="bold">Temperature</p>
                        <p class="temperature"></p>
                        <p class="bold">Clouds</p>
                        <p class="clouds"></p>
                        <p class="bold">Wind</p>
                        <img name="wind" class="text-center" src="" data-toggle="tooltip" data-placement="top" title=""></img>
                        <img name="wind-2" class="text-center" src="" data-toggle="tooltip" data-placement="top" title="" hidden></img>
                        <p class="wind-speed"></p>
                        <p class="bold">Precipitation</p>
                        <p class="precipitation-type"></p>
                        <p class="precipitation-value"></p>
                        <p class="bold">Pressure</p>
                        <p class="pressure"></p>
                        <p class="bold">Humidity</p>
                        <p class="humidity"></p>
                    </div>
                    <div class="time-div time-2" hidden>
                        <p class="weather-time text-center"></p>
                        <hr>
                        <p class="bold">Weather</p>
                        <img name="weather" class="text-center" src="" data-toggle="tooltip" data-placement="top" title=""></img>
                        <p class="bold">Temperature</p>
                        <p class="temperature"></p>
                        <p class="bold">Clouds</p>
                        <p class="clouds"></p>
                        <p class="bold">Wind</p>
                        <img name="wind" class="text-center" src="" data-toggle="tooltip" data-placement="top" title=""></img>
                        <img name="wind-2" class="text-center" src="" data-toggle="tooltip" data-placement="top" title="" hidden></img>
                        <p class="wind-speed"></p>
                        <p class="bold">Precipitation</p>
                        <p class="precipitation-type"></p>
                        <p class="precipitation-value"></p>
                        <p class="bold">Pressure</p>
                        <p class="pressure"></p>
                        <p class="bold">Humidity</p>
                        <p class="humidity"></p>
                    </div>
                    <div class="time-div time-3" hidden>
                        <p class="weather-time text-center"></p>
                        <hr>
                        <p class="bold">Weather</p>
                        <img name="weather" class="text-center" src="" data-toggle="tooltip" data-placement="top" title=""></img>
                        <p class="bold">Temperature</p>
                        <p class="temperature"></p>
                        <p class="bold">Clouds</p>
                        <p class="clouds"></p>
                        <p class="bold">Wind</p>
                        <img name="wind" class="text-center" src="" data-toggle="tooltip" data-placement="top" title=""></img>
                        <img name="wind-2" class="text-center" src="" data-toggle="tooltip" data-placement="top" title="" hidden></img>
                        <p class="wind-speed"></p>
                        <p class="bold">Precipitation</p>
                        <p class="precipitation-type"></p>
                        <p class="precipitation-value"></p>
                        <p class="bold">Pressure</p>
                        <p class="pressure"></p>
                        <p class="bold">Humidity</p>
                        <p class="humidity"></p>
                    </div>
                    <div class="time-div time-4" hidden>
                        <p class="weather-time text-center"></p>
                        <hr>
                        <p class="bold">Weather</p>
                        <img name="weather" class="text-center" src="" data-toggle="tooltip" data-placement="top" title=""></img>
                        <p class="bold">Temperature</p>
                        <p class="temperature"></p>
                        <p class="bold">Clouds</p>
                        <p class="clouds"></p>
                        <p class="bold">Wind</p>
                        <img name="wind" class="text-center" src="" data-toggle="tooltip" data-placement="top" title=""></img>
                        <img name="wind-2" class="text-center" src="" data-toggle="tooltip" data-placement="top" title="" hidden></img>
                        <p class="wind-speed"></p>
                        <p class="bold">Precipitation</p>
                        <p class="precipitation-type"></p>
                        <p class="precipitation-value"></p>
                        <p class="bold">Pressure</p>
                        <p class="pressure"></p>
                        <p class="bold">Humidity</p>
                        <p class="humidity"></p>
                    </div>
                    <div class="time-div time-5" hidden>
                        <p class="weather-time text-center"></p>
                        <hr>
                        <p class="bold">Weather</p>
                        <img name="weather" class="text-center" src="" data-toggle="tooltip" data-placement="top" title=""></img>
                        <p class="bold">Temperature</p>
                        <p class="temperature"></p>
                        <p class="bold">Clouds</p>
                        <p class="clouds"></p>
                        <p class="bold">Wind</p>
                        <img name="wind" class="text-center" src="" data-toggle="tooltip" data-placement="top" title=""></img>
                        <img name="wind-2" class="text-center" src="" data-toggle="tooltip" data-placement="top" title="" hidden></img>
                        <p class="wind-speed"></p>
                        <p class="bold">Precipitation</p>
                        <p class="precipitation-type"></p>
                        <p class="precipitation-value"></p>
                        <p class="bold">Pressure</p>
                        <p class="pressure"></p>
                        <p class="bold">Humidity</p>
                        <p class="humidity"></p>
                    </div>
                    <div class="time-div time-6" hidden>
                        <p class="weather-time text-center"></p>
                        <hr>
                        <p class="bold">Weather</p>
                        <img name="weather" class="text-center" src="" data-toggle="tooltip" data-placement="top" title=""></img>
                        <p class="bold">Temperature</p>
                        <p class="temperature"></p>
                        <p class="bold">Clouds</p>
                        <p class="clouds"></p>
                        <p class="bold">Wind</p>
                        <img name="wind" class="text-center" src="" data-toggle="tooltip" data-placement="top" title=""></img>
                        <img name="wind-2" class="text-center" src="" data-toggle="tooltip" data-placement="top" title="" hidden></img>
                        <p class="wind-speed"></p>
                        <p class="bold">Precipitation</p>
                        <p class="precipitation-type"></p>
                        <p class="precipitation-value"></p>
                        <p class="bold">Pressure</p>
                        <p class="pressure"></p>
                        <p class="bold">Humidity</p>
                        <p class="humidity"></p>
                    </div>
                    <div class="time-div time-7" hidden>
                        <p class="weather-time text-center"></p>
                        <hr>
                        <p class="bold">Weather</p>
                        <img name="weather" class="text-center" src="" data-toggle="tooltip" data-placement="top" title=""></img>
                        <p class="bold">Temperature</p>
                        <p class="temperature"></p>
                        <p class="bold">Clouds</p>
                        <p class="clouds"></p>
                        <p class="bold">Wind</p>
                        <img name="wind" class="text-center" src="" data-toggle="tooltip" data-placement="top" title=""></img>
                        <img name="wind-2" class="text-center" src="" data-toggle="tooltip" data-placement="top" title="" hidden></img>
                        <p class="wind-speed"></p>
                        <p class="bold">Precipitation</p>
                        <p class="precipitation-type"></p>
                        <p class="precipitation-value"></p>
                        <p class="bold">Pressure</p>
                        <p class="pressure"></p>
                        <p class="bold">Humidity</p>
                        <p class="humidity"></p>
                    </div>
                </div>
            </div>
        </div>
        <script src="{{ asset('js/jquery-3.3.1.min.js') }}"></script>
        <script src="{{ asset('bootstrap/js/bootstrap.min.js') }}"></script>
        <script src="{{ asset('jquery-validation/dist/jquery.validate.min.js') }}"></script>
        <!-- Ajax request -->
        <script src="{{ asset('js/weather.js') }}"></script>
    </body>
</html>
