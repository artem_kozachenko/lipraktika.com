<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWeatherTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('weather', function (Blueprint $table) {
            $table->increments('id');
            $table->string('city_id');
            $table->string('date');
            $table->string('time');
            $table->string('sunrise');
            $table->string('sunset');
            $table->string('weather');
            $table->string('weather_img');
            $table->string('precipitation_value')->nullable();
            $table->string('precipitation_type')->nullable();
            $table->string('wind_speed');
            $table->string('wind_direction')->nullable();
            $table->string('temperature');
            $table->string('pressure');
            $table->string('humidity');
            $table->string('clouds');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('weather');
    }
}
