function ucfirst(str) {
    var f = str.charAt(0).toUpperCase();
    return f + str.substr(1, str.length - 1);
}
$('#city').focus(function() {
    $('.alert-danger').remove();
});
$.validator.addMethod('english', function(value, element) {
    return this.optional(element) || /[a-zA-Z-]{1,}/.test(value);
}, 'Only Latin letters are allowed.');
$('#city_form').validate({
    errorElement: 'p',
    rules: {
        city: {
            required: true,
            english: true
        },
    },
    messages: {
        city: {
            required: 'The city name field is required.',
        }
    },
    submitHandler: function(form) {
        $('#preloader').show();
        $.ajax( {
            url: form.action,
            data:  $(form).serialize(),
            type: form.method,
            dataType: 'json',
            success: function(response) {
                if(response != 0) {
                    $('.alert-success').remove();
                    $('#message').append('<p class="alert alert-success text-center">' + response[0] +'</p>');
                    $('.alert-success').delay(500).show(10, function() {
                        $(this).delay(1500).hide(10, function() {
                            $(this).remove();
                        });
                    });
                    $('div.active').removeClass('active');
                    $('li.active').removeClass('active');
                    // ЦИКЛ ДЛЯ РАЗБИЯНИЯ МАССИВА НА ДНИ
                    for(var i = 0; i < 5; i++) {
                        var day = response[1][i];
                        var currentDay = $('#day_' + i);
                        $(currentDay).children('.sun').html('<p><b>Sunrise: </b>' + response[1][0][0].sunrise + '<b> Sunset: </b>' + response[1][0][0].sunset + '</p>');
                        $('#tab_' + (i + 1)).text(day[0].date);
                        // ЦИКЛ ДЛЯ РАЗБИЯНИЯ ДНЕЙ НА ВРЕМЯ
                        day.forEach(function(time, n, day) {
                            var currentTime = $(currentDay).children('.time-' + n);
                            $(currentTime).removeAttr('hidden');
                            $(currentTime).children('.weather-time').text(time.time);
                            $(currentTime).children('img[name="weather"]').attr('src', time.weather_img);
                            $(currentTime).children('img[name="weather"]').attr('title', ucfirst(time.weather));
                            $(currentTime).children('.temperature').text(time.temperature);
                            $(currentTime).children('.clouds').text(time.clouds + '%');
                            if(time.wind_direction != null) {
                                index = time.wind_direction.indexOf('-');
                                if(index == '-1') {
                                    $(currentTime).children('img[name="wind"]').attr('title', time.wind_direction);
                                    $(currentTime).children('img[name="wind"]').attr('src', 'http://lipraktika.com/img/' + time.wind_direction + '.png.');
                                }
                                else {
                                    firstDirection = time.wind_direction.substring(0, index);
                                    secondDirection = time.wind_direction.substring(index + 1);
                                    $(currentTime).children('img[name="wind"]').attr('src', 'http://lipraktika.com/img/' + firstDirection + '.png.');
                                    $(currentTime).children('img[name="wind"]').attr('title', ucfirst(firstDirection));
                                    $(currentTime).children('img[name="wind-2"]').removeAttr('hidden');
                                    $(currentTime).children('img[name="wind-2"]').attr('src', 'http://lipraktika.com/img/' + secondDirection + '.png.');
                                    $(currentTime).children('img[name="wind-2"]').attr('title', ucfirst(secondDirection));
                                }
                            }
                            else {
                                $(currentTime).children('img[name="wind"]').attr('title', 'Anywhere');
                                $(currentTime).children('img[name="wind"]').attr('src', 'http://lipraktika.com/img/anywhere.png.');
                            }
                            $(currentTime).children('.wind-speed').text(time.wind_speed + ' m/s');
                            if(time.precipitation_type != null || time.precipitation_value != null) {
                                $(currentTime).children('.precipitation-type').text(ucfirst(time.precipitation_type));
                                $(currentTime).children('.precipitation-value').text(Math.ceil(time.precipitation_value * 10000)/10000 + ' mm');
                            }
                            else{
                                $(currentTime).children('.precipitation-type').text('No');
                                $(currentTime).children('.precipitation-value').text('0 mm');
                            }
                            $(currentTime).children('.pressure').text(time.pressure + ' hPa');
                            $(currentTime).children('.humidity').text(time.humidity + '%');
                        });
                    }
                    $('li.disabled').removeClass('disabled');
                    $('li').has('#tab_1').addClass('active');
                    $('#day_0').addClass('active');
                    $('#preloader').hide();
                }
                else {
                    $('.alert-danger').remove();
                    $('div.active').removeClass('active');
                    $('li.active').removeClass('active');
                    $('li').addClass('disabled');
                    for (var i = 1; i < 5; i++) {
                        $('#tab_' + i).text('Day ' + i);
                    }
                    $('#message').append(
                        '<p class="alert alert-danger text-center">'+
                            'Wrong city name!'+
                        '</p>'
                    );
                    $('#preloader').hide();
                }
            },
        })
    }
});
